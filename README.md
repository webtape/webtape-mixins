# Documentation:

## List of mixins:
1. **[placeholder](#placeholder)** `form`
2. **[center-block](#placeholder)** `layout`
3. **[pos-abs](#pos-abs)** `layout`
4. **[pos-rel](#pos-rel)** `layout`
5. **[nav-list](#nav-list)** `layout`
6. **[font-rem](#font-rem)** `layout`
7. **[size](#size)** `layout`
8. **[vert-align](#vert-align)** `layout`
9. **[reset-lists](#reset-lists)** `reset`
10. **[hide-text](#hide-text)** `reset`
11. **[reset-mar-pad](#reset-mar-pad)** `reset`
12. **[reset-element](#reset-element)** `reset`
13. **[reset-table](#reset-table)** `reset`
14. **[text-selection](#text-selection)**
15. **[letterpress](#letterpress)**
16. **[box-embess](#box-embess)**
17. **[triangle](#triangle)**
18. **[gradient-button](#gradient-button)**
19. **[ghost-valign](#ghost-valign)**
20. **[truncate-text](#truncate-text)**
21. **[animation](#animation)**
22. **[haf](#haf)**
23. **[prefix](#prefix)**

## List of helpers:
1. **[black](#black)**
2. **[white](#white)**
3. **[strip-units](#strip-units)**
4. **[em](#em)**
5. **[rem](#rem)**
6. **[result](#result)**

***

## **placeholder**

**Info:**

Outputs vendor-prefixed styling for placeholders.

**Arguments:**

* color

**Tips & Tricks:**

Only use it on form elements like input fields.

**Example:**

```
#!scss
input.txt {
	@include placeholder(#000);
}
```

 

## **center-block**

**Info:**

Center a block element within it’s parent container.

**Arguments:**

* width (default: false)

**Tips & Tricks:**

You can give the element a width if you want.

**Example:**

```
#!scss
image {
    @include center-block(100px):
}
```

 

## **pos-abs**

**Info:**

Position an element absolute.

**Arguments:**

* top (default: auto)
* right (default: auto)
* bottom (default: auto)
* left (default: auto)

**Tips & Tricks:**

/

**Example:**

```
#!scss
p.tooltip {
    @include pos-abs($top: 10px, $left: 10px):
}
```

 

## **pos-rel**

**Info:**

Position an element relative.

**Arguments:**

* top (default: auto)
* right (default: auto)
* bottom (default: auto)
* left (default: auto)

**Tips & Tricks:**

/

**Example:**

```
#!scss
p.tooltip {
    @include pos-rel:
}
```

 

## **nav-list**

**Info:**

Position list-items horizontally. Very fast when creating an horizontal navigation.

**Arguments:**

/

**Tips & Tricks:**

It will also remove list-style images.

**Example:**

```
#!scss
ul.nav {
    @include nav-list:
}
```

 

## **font-rem**

**Info:**

Set your font-size in rem and provide a fallback for older browsers.

**Arguments:**

* px
* base (default: 16)

**Tips & Tricks:**

Choose 16px as the global font-size and you don’t have to use the base argument.

**Example:**

```
#!scss
h2 {
    @include font-rem(32px):
}
```

 

## **size**

**Info:**

Give an element a specific width and/or height.

**Arguments:**

* width (default: false)
* height (default: false)

**Tips & Tricks:**

/

**Example:**

```
#!scss
div.rectangle {
    @include size(300px, 50px):
}
```

 

## **vert-align**

**Info:**

Vertical align two elements.

**Arguments:**

* child (default: false)
* align (default: middle)

**Tips & Tricks:**

It’s important to define use the child argument.

**Example:**

```
#!scss
article {
    @include vert-align(image):
}
```

 

## **reset-list**

**Info:**

Remove (default) list-item images.

**Arguments:**

/

**Tips & Tricks:**

Use this globally.

**Example:**

```
#!scss
ul, ol {
    @include reset-list;
}
```

 

## **hide-text**

**Info:**

Hide text when using image replacement.

**Arguments:**

/

**Tips & Tricks:**

/

**Example:**

```
#!scss
h1.logo {
     @include hide-text;
}
```

 

## **reset-mar-pad**

**Info:**

Reset the margins and paddings from a specific element.

**Arguments:**

/

**Tips & Tricks:**

Use this globally.

**Example:**

```
#!scss
image {
    @include reset-mar-pad;
}
```

 

## **reset-element**

**Info:**

This mixin resets margin, padding, font, alignment, border and background.

**Arguments:**

/

**Tips & Tricks:**

Use this globally.

**Example:**

```
#!scss
p {
    @include reset-element
}
```

 

## **reset-table**

**Info:**

This mixin resets table specific properties like margin, padding, border-collapse, border-spacing, background, ...

**Arguments:**

/

**Tips & Tricks:**

Use this globally.

**Example:**

```
#!scss
table {
    @include reset-table
}
```

 

## **text-selection**

**Info:**

Override the default text selection color.

**Arguments:**

* color
* text (default: false)

**Tips & Tricks:**

It’s possible to change te font-color ass well. Use the text argument to set a specific color.

**Example:**

```
#!scss
p {
	@include text-selection(#fff2a8);
}
```

 

## **letterpress**

**Info:**

Create a letterpress effect using text-shadow.

**Arguments:**

* opacity (default: 1)

**Tips & Tricks:**

It’s useless on a white background.

**Example:**

```
#!scss
h1 {
    @include letterpress(0.75);
}
```

 

## **box-emboss**

**Info:**

Create a emboss effect using box-shadow.

**Arguments:**

* opac-wh (default: 1)
* opac-black (default: 1)

**Tips & Tricks:**

This will not work as a text effect.

**Example:**

```
#!scss
div.emboss {
    @include box-emboss(0.80, 0.60);
}
```

 

## **triangle**

**Info:**

Create a css based triangle.

**Arguments:**

* color (default: #000)
* size (default: 50px)
* point (default: top)

**Tips & Tricks:**

You can use the triangle as a pointer for your tooltips.

**Example:**

```
#!scss
div.pointer:after {
    @include triangle(#FFF, 30px, bottom);
}
```

 

## **gradient-button**

**Info:**

It will transform your flat-color button to a gradient button with a black-to-white gradient overlay.

**Arguments:**

* bg
* text (default: #FFF)
* input (default: false)

**Tips & Tricks:**

Use the input argument when using it on a submit button.

**Example:**

```
#!scss
input[type=“submit"] {
    @include gradient-button(#3093c7, #FFF, TRUE);
}
```

 

## **ghost-valign**

**Info:**

Vertical align two elements width a dynamic height.

**Arguments:**

/

**Tips & Tricks:**

/

**Example:**

```
#!scss
article {
    @include ghost-valign();
}
```

 

## **truncate-text**

**Info:**

This will truncate text and adding something to break nicely.

**Arguments:**

* overflow (default: ellipsis)

**Tips & Tricks:**

You can only choose between clip, ellipsis (default) or a string.

**Example:**

```
#!scss
p.summary {
	@include truncate-text;
}
```

 

## **animation**

**Info:**

This mixin simplifies your compass animations.

**Arguments:**

* property (all)
* duration (0.15s)
* timing (linear)

**Tips & Tricks:**

Use this mixin on your buttons and create a smooth hover animation.

**Example:**

```
#!scss
button, a {
    @include animation(color, 0.1s, ease-in);
}
```

 

## **haf**

**Info:**

In most cases, you will use the same property values for the hover, the active and the focus state.

**Arguments:**

It makes use of the @content directive

**Tips & Tricks:**

haf = hover, active, focus

**Example:**

```
#!scss
button, a {
    @include animation(color, 0.1s, ease-in);
}
```

 

## **prefix**

**Info:**

Vendorize any property you want.

**Arguments:**

* property
* value

**Tips & Tricks:**

First check if there is a specific compass mixing (i.e border-radius) before you vendorize using this mixin.

**Example:**

```
#!scss
div.rounded {
	@include prefix(‘box-sizing’, ‘border-box');
}
```

 

## **black**

**Info:**

Define a semi-transparent black color.

**Arguments:**

* opacity (default: 1)

**Tips & Tricks:**

/

**Example:**

```
#!scss
a {
    color: black(0.8);
}
```

 

## **white**

**Info:**

Define a semi-transparent white color.

**Arguments:**

* opacity (default: 1)

**Tips & Tricks:**

/

**Example:**

```
#!scss
a:hover {
     color: white(0.8);
}
```

 

## **strip-units**

**Info:**

Strip number units when necessary.

**Arguments:**

* number

**Tips & Tricks:**

Very useful when calculation with numbers.
It’s used in other helpers i.e rem()

**Example:**

```
#!scss

```

 

## **em**

**Info:**

Convert pixels to em.

**Arguments:**

* px
* base (default: 16)

**Tips & Tricks:**

/

**Example:**

```
#!scss
h1 {
    font-size: em(48px, 18px);
}
```

  
 
## **rem**

**Info:**

Convert pixels to rem.

**Arguments:**

* px
* base (default: 16)

**Tips & Tricks:**

Choose 16px as the global font-size and you don’t have to use the base argument.

**Example:**

```
#!scss
h3 {
    font-size: rem(24px);
}
```

  
 
## **result**

**Info:**

Make use of the famous formula: target / context = result.

**Arguments:**

* trg
* cnt

**Tips & Tricks:**

Very useful for converting fixed design to a responsive lay-out

**Example:**

```
#!scss
div.logo {
    width: result(200, 960);
}
```